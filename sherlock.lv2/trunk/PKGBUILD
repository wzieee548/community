# Maintainer: David Runge <dvzrv@archlinux.org>

pkgname=sherlock.lv2
pkgver=0.22.0
pkgrel=1
pkgdesc="An investigative plugin bundle"
arch=('x86_64')
url="https://open-music-kontrollers.ch/lv2/sherlock/"
license=('Artistic2.0')
groups=('lv2-plugins' 'pro-audio')
depends=('glibc' 'libglvnd' 'libx11')
makedepends=('flex' 'glu' 'lv2' 'meson' 'serd' 'sord' 'sratom')
checkdepends=('lv2lint')
source=("https://git.open-music-kontrollers.ch/lv2/$pkgname/snapshot/$pkgname-$pkgver.tar.xz")
sha512sums=('3ed6caa88ac8d89e4f6c4934966c4c0988597152dfd6a963057d7bf585cbe294539398303d76986736dcc0683ea304f77f668ee201d6871f527430476d60f92e')

build() {
  cd "$pkgname-$pkgver"
  arch-meson --prefix=/usr \
             --libdir=lib \
             --buildtype=release \
             build
  ninja -C build
}

check() {
  cd "$pkgname-$pkgver"
  _plugin_names=('atom_inspector' 'midi_inspector' 'osc_inspector')
  for _plugin in ${_plugin_names[@]}; do
    lv2lint -Mpack -I "build/" "http://open-music-kontrollers.ch/lv2/sherlock#${_plugin}"
  done
}

package() {
  depends+=('libserd-0.so' 'libsratom-0.so')
  cd "$pkgname-$pkgver"
  DESTDIR="${pkgdir}" meson install -C build
  # docs
  install -vDm 644 {ChangeLog,README.md} \
    -t "${pkgdir}/usr/share/doc/${pkgname}"
}
