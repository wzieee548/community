# Maintainer: kpcyrd <kpcyrd[at]archlinux[dot]org>
# Contributor: NOGISAKA Sadata <ngsksdt@gmail.com>
# Contributor: Bennett Piater <bennett at piater dot name>

pkgname=doctl
pkgver=1.45.1
pkgrel=2
pkgdesc='The official command line interface for the DigitalOcean API'
url='https://github.com/digitalocean/doctl'
arch=('x86_64')
license=('Apache')
depends=('glibc')
makedepends=('go')
source=(https://github.com/digitalocean/${pkgname}/archive/v${pkgver}/${pkgname}-${pkgver}.tar.gz)
sha512sums=('90a511a7a87f46a95c0e1a7ee3283066e03627dc8dca4d16d444eab01675b99bc67c5ccd041604dc2ecb94b4d224769b10f7a11ab502db423aae02d43e8264c6')
b2sums=('e1103c3c2393feec482d61573cdf8c0267d38b9c965e052ce01189e3f3d6443aeb6521c68ecb43656b0f0f0112db3198f28e77853f1cbcdf8bbdd3b2038b5411')

build() {
  cd ${pkgname}-${pkgver}
  go build \
    -trimpath \
    -buildmode=pie \
    -mod=readonly \
    -ldflags "-extldflags \"${LDFLAGS}\"" \
    ./cmd/...
}

package() {
  cd ${pkgname}-${pkgver}
  install -Dm 755 "${pkgname}" -t "${pkgdir}/usr/bin"

  # setup completions
  install -dm 755 "${pkgdir}/usr/share/bash-completion/completions" \
                  "${pkgdir}/usr/share/zsh/site-functions" \
                 "${pkgdir}/usr/share/fish/vendor_completions.d"
  "${pkgdir}/usr/bin/${pkgname}" completion bash > "${pkgdir}/usr/share/bash-completion/completions/${pkgname}"
  "${pkgdir}/usr/bin/${pkgname}" completion zsh > "${pkgdir}/usr/share/zsh/site-functions/_${pkgname}"
  "${pkgdir}/usr/bin/${pkgname}" completion fish > "${pkgdir}/usr/share/fish/vendor_completions.d/${pkgname}.fish"
}

# vim: ts=2 sw=2 et:
