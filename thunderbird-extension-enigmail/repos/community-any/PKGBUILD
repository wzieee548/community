# Maintainer: Eli Schwartz <eschwartz@archlinux.org>
# Maintainer: Jonas Witschel <diabonas@archlinux.org>
# Contributor: Daniel Landau <daniel.landau@iki.fi>
# Contributor: Einhard Leichtfuß <alguien@respiranto.de>
# Contributor: Xyne
# Contributor: David Manouchehri <d@32t.ca>
# Contributor: Alexander Fehr <pizzapunk gmail com>
# Contributor: Thomas Jost <schnouki schnouki net>
# Contributor: Hinrich Harms <arch hinrich de>

pkgname=thunderbird-extension-enigmail
pkgver=2.1.7
pkgrel=1
pkgdesc="OpenPGP message encryption and authentication for Thunderbird"
arch=('any')
url="https://www.enigmail.net/"
license=('MPL' 'GPL3')
makedepends=('zip' 'python' 'perl')
replaces=('thunderbird-enigmail')
source=("https://www.enigmail.net/download/source/enigmail-${pkgver}.tar.gz"{,.asc}
        "0001-preferences-disable-pEpAutoDownload-by-default.patch"
        "0001-Disable-Thunderbird-78-upgrade-warning-message.patch")
sha512sums=('1b57091c8ab9aaa086f327b78d904d688c850b6d39e37e2dac82e0629c0279723eae4608ecd08a24efe9ed1bdc86fbc497e97cd800c7349a70612a42b98f3e41'
            'SKIP'
            'baebd963400574db89be747a4419534f945bdc64136d4014656ff98a9615a23984bca724da3f3840670979aab08ce441eee067921e21d0cb216938a20ed785b2'
            '4ddf887765e4296b3c639748d875b179d1e2a5fb38ad16e2918f115a9ff9a05e2f9c66218544f7ab8189f096908df761d4047fd5d23972c02737e46c4a0c843c')
b2sums=('8f6d1ec16b48219c75c6dbcddf4807ed57965eeec29776e7c757d5aa34da6bfdbbb58964ee3d7de2efcb65ab69fa5b020f1a8ec01cd8eee662d8195a217cdc69'
        'SKIP'
        'c593ed7b094d9feecb2f14624cf0628ab390c96f0fb0212ab0069333508b59057ef4b0518da1bf59eb8aaf0942303c4c45afab76d0b8e77a93763eab975cb4c0'
        'a2ba38e56f14a87834023076a75a6c59bc17488104227d8db3e31072f2dcc6488808a980b4073111dec4cf4661349c3e995b8226808c3038d96f2cab666eb90b')
validpgpkeys=('4F9F89F5505AC1D1A260631CDB1187B9DD5F693B') # Patrick Brunschwig <patrick@enigmail.net>

prepare() {
    cd "${srcdir}"/enigmail

    # Using vendor settings via /usr/lib/thunderbird/defaults/preferences/enigmail.js
    # does not seem to work.
    patch -p1 -i ../0001-preferences-disable-pEpAutoDownload-by-default.patch

    # Disable warning message in favour of a versioned dependency to discourage partial upgrades
    patch -p1 -i ../0001-Disable-Thunderbird-78-upgrade-warning-message.patch
}

build() {
    cd "${srcdir}"/enigmail

    ./configure
    make -j1 # fails with -j greater than 1
}

package() {
    depends=('thunderbird>=68' 'thunderbird<78' 'gnupg')
    cd "${srcdir}"/enigmail

    if ! _extension_id="$(sed -n '/.*<em:id>\(.*\)<\/em:id>.*/{s//\1/p;q}' build-tb/dist/install.rdf 2>/dev/null)" ||
            [[ -z $_extension_id ]]; then
        _extension_id="$(sed -n 's/.*"id": "\(.*\)".*/\1/p' build-tb/dist/manifest.json)"
    fi
    _extension_dest="${pkgdir}/usr/lib/thunderbird/extensions/${_extension_id}"
    # Should this extension be unpacked or not?
    if grep -q '<em:unpack>true</em:unpack>' build-tb/dist/install.rdf 2>/dev/null; then
        install -dm755 "${_extension_dest}"
        cp -R build-tb/dist/* "${_extension_dest}"
        chmod -R ugo+rX "${_extension_dest}"
    else
        install -Dm644 build-tb/enigmail-${pkgver}.xpi "${_extension_dest}.xpi"
    fi
}
