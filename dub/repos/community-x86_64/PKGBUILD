# Maintainer: Dan Printzell <arch@vild.io>
# Maintainer: Filipe Laíns (FFY00) <lains@archlinux.org>
# Contributor: Mihails Strasuns <public@dicebot.lv>
# Contributor: Moritz Maxeiner <moritz@ucworks.org>

pkgname=dub
pkgver=1.21.0
pkgrel=1
pkgdesc="Developer package manager for D programming language"
arch=('x86_64')
url="https://github.com/dlang/dub"
license=('MIT')
groups=('dlang')
makedepends=('ldc')
depends=('liblphobos' 'curl')
source=("$pkgname-$pkgver.tar.gz::https://github.com/dlang/dub/archive/v$pkgver.tar.gz")
sha512sums=('d85be6e35f51e34d34afce71fe902b8ef623c6f49ed6f5f755dded5bceda49416f9d625377136b928d0f95496e57999f0dd57a4cea796360edd34c72556debd7')

build() {
	cd "$pkgname-$pkgver"

	echo Generating version file...
	echo "module dub.version_;" > source/dub/version_.d
	echo "enum dubVersion = \"$pkgver\";" >> source/dub/version_.d

	ldmd2 -ofbin/dub -release -O5 -version=DubUseCurl -Isource -L-lcurl -L="$LDFLAGS" -release -flto=full -linker=gold -link-defaultlib-shared=false -L--build-id @build-files.txt
}

package() {
	# binaries
	install -Dm755 "$pkgname-$pkgver/bin/dub" "$pkgdir/usr/bin/dub"

	# license
	install -Dm644 "$pkgname-$pkgver/LICENSE" "$pkgdir/usr/share/licenses/$pkgname/LICENSE"

	# bash, fish and zsh completion
	install -Dm644 "$pkgname-$pkgver/scripts/bash-completion/dub.bash" "$pkgdir/usr/share/bash-completion/completions/dub"
	install -Dm644 "$pkgname-$pkgver/scripts/fish-completion/dub.fish" "$pkgdir/usr/share/fish/vendor_completions.d/dub.fish"
	install -Dm644 "$pkgname-$pkgver/scripts/zsh-completion/_dub" "$pkgdir/usr/share/zsh/site-functions/_dub"
}
