# Maintainer: David Runge <dvzrv@archlinux.org>

pkgname=patchmatrix
pkgver=0.18.0
pkgrel=1
pkgdesc="A JACK patchbay in flow matrix style"
arch=('x86_64')
url="https://git.open-music-kontrollers.ch/lad/patchmatrix/about/"
license=('Artistic2.0')
groups=('lv2-plugins' 'pro-audio')
depends=('glibc' 'hicolor-icon-theme' 'libglvnd' 'libx11')
makedepends=('glu' 'jack' 'lv2' 'meson')
source=("https://git.open-music-kontrollers.ch/lad/${pkgname}/snapshot/${pkgname}-${pkgver}.tar.xz")
sha512sums=('08125b84cdd653ec9ab0d54769095216b84349d256911ddf82641f748e61d3f66a0e2bcee2eed2689e80de9852a247aa9eec7fd1ec98635b3113b6d0d18a2e62')

build() {
  cd "$pkgname-$pkgver"
  arch-meson --prefix=/usr \
             --libdir=lib \
             --buildtype=release \
             build
  ninja -C build
}

check() {
  cd "$pkgname-$pkgver"
  meson test -C build
}

package() {
  depends+=('libjack.so')
  cd "$pkgname-$pkgver"
  DESTDIR="${pkgdir}" meson install -C build
  # docs
  install -vDm 644 {ChangeLog,README.md} \
    -t "${pkgdir}/usr/share/doc/${pkgname}"
}
