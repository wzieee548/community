# Maintainer: Felix Yan <felixonmars@archlinux.org>

pkgname=python-yaspin
pkgver=0.17.0
pkgrel=1
pkgdesc="Yet Another Terminal Spinner"
url="https://github.com/pavdmyt/yaspin"
license=('MIT')
arch=('any')
depends=('python')
makedepends=('python-dephell')
checkdepends=('python-pytest-runner')
source=("$pkgname-$pkgver.tar.gz::https://github.com/pavdmyt/yaspin/archive/v$pkgver.tar.gz")
sha512sums=('e059737d8f2b46929c8044ade0203d9f1298fc42a7354a984f385de5a72ebee06f2d7cbccb6c692c283b76139b825399901047ac0d6899147cf7a9532cc5b142')

prepare() {
  cd yaspin-$pkgver

  # poetry-generated setup.py is badly broken in several ways, including
  # distribution of tests in the built package as well as using distutils for
  # bad metadata. See https://github.com/sdispater/poetry/issues/866
  dephell deps convert --from pyproject.toml --to setup.py
}

build() {
  cd yaspin-$pkgver
  python setup.py build
}

check() {
  cd yaspin-$pkgver
  python setup.py pytest
}

package() {
  cd yaspin-$pkgver
  python setup.py install --root="$pkgdir" --optimize=1

  install -Dm644 LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname/
}
