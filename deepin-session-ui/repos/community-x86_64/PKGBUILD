# Maintainer: Felix Yan <felixonmars@archlinux.org>
# Contributor: Josip Ponjavic <josipponjavic at gmail dot com>
# Contributor: Xu Fasheng <fasheng.xu[AT]gmail.com>

pkgname=deepin-session-ui
pkgver=5.2.0.11
pkgrel=1
pkgdesc='Deepin desktop-environment - Session UI module'
arch=('x86_64')
url="https://github.com/linuxdeepin/dde-session-ui"
license=('GPL3')
groups=('deepin')
depends=('gsettings-qt' 'deepin-qt5integration' 'liblightdm-qt5' 'qt5-svg' 'deepin-daemon'
         'deepin-control-center' 'deepin-wallpapers')
makedepends=('deepin-gettext-tools' 'qt5-tools')
provides=('deepin-notifications')
conflicts=('dde-workspace' 'dde-session-ui' 'deepin-notifications')
replaces=('dde-workspace' 'dde-session-ui' 'deepin-notifications')
source=("$pkgname-$pkgver.tar.gz::https://github.com/linuxdeepin/dde-session-ui/archive/$pkgver.tar.gz"
         deepin-session-ui-qt5.15.patch)
sha512sums=('03d9dcb1dd8afe977fb3315839784c02f8efc4ea6a5de07abce3b6dcfd186de587324e77352c6022600e1cfcafdafde6946a11ca23f020cd5a53d08af8b1a860'
            '846726964cac5005b0ac3a5043e5df914ff83faff68f98d2513d86494a9718a85ae4268776fd08ffe852e28ac0ae5878353d3c65db84fbd1509b2325e70fe8d6')

prepare() {
  cd dde-session-ui-$pkgver
  sed -i 's|/usr/share/backgrounds/default_background.jpg|/usr/share/backgrounds/deepin/desktop.jpg|' widgets/*.cpp

  sed -i '/include <QPainter>/a #include <QPainterPath>' dde-notification-plugin/notifications/notificationswidget.cpp

  patch -p1 -i ../deepin-session-ui-qt5.15.patch # Fix build with Qt 5.15
}

build() {
  cd dde-session-ui-$pkgver
  qmake-qt5 PREFIX=/usr
  make
}

package() {
  cd dde-session-ui-$pkgver
  make INSTALL_ROOT="$pkgdir" install
}
