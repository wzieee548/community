# Maintainer: Balló György <ballogyor+arch at gmail dot com>
# Maintainer: Bruno Pagani <archange@archlinux.org>

pkgname=keepassxc
pkgver=2.5.4
pkgrel=2
pkgdesc="Cross-platform community-driven port of Keepass password manager"
arch=(x86_64)
url="https://keepassxc.org/"
license=(GPL)
depends=(argon2 curl libgcrypt libsodium libxtst qrencode
         qt5-svg qt5-x11extras quazip yubikey-personalization)
makedepends=(cmake qt5-tools)
optdepends=('xclip: keepassxc-cli clipboard support')
provides=(org.freedesktop.secrets)
source=(https://github.com/keepassxreboot/keepassxc/releases/download/$pkgver/keepassxc-$pkgver-src.tar.xz{,.sig})
sha256sums=('a55e0801c318b02b1ac4e16e9b7a87ccfa7b039ea60d2c62610bd1bbbdd6cd4a' 'SKIP')
# List of signing keys can be found at https://keepassxc.org/verifying-signatures/
validpgpkeys=(BF5A669F2272CF4324C1FDA8CFB4C2166397D0D2
              71D4673D73C7F83C17DAE6A2D8538E98A26FD9C4
              AF0AEA44ABAC8F1047733EA7AFF235EEFB5A2517
              C1E4CBA3AD78D3AFD894F9E0B7A66F03B59076A8)

build() {
    cmake -S keepassxc-$pkgver -B build \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=/usr \
        -DCMAKE_INSTALL_LIBDIR=lib \
        -DWITH_XC_ALL=ON \
        -DWITH_XC_UPDATECHECK=OFF
    cmake --build build
}

check() {
    cmake --build build --target test
}

package() {
    cmake --build build --target install -- DESTDIR="$pkgdir"
}
