# Maintainer: Felix Yan <felixonmars@archlinux.org>

pkgname=eslint
pkgver=7.4.0
pkgrel=1
pkgdesc='An AST-based pattern checker for JavaScript'
arch=('any')
url='https://eslint.org'
license=('MIT')
depends=('nodejs' 'acorn')
makedepends=('npm')
source=("https://registry.npmjs.org/$pkgname/-/$pkgname-$pkgver.tgz")
noextract=("$pkgname-$pkgver.tgz")
sha512sums=('814fa5c6194f1eee391f726410681885686447dc0b1c71170bd15b5859d394491b2b264a5a05912e07fad44f335a606e23a839c4a06987d96d83736d64c545f2')

package() {
  npm install -g --user root --prefix "$pkgdir"/usr "$srcdir"/$pkgname-$pkgver.tgz

  # Non-deterministic race in npm gives 777 permissions to random directories.
  # See https://github.com/npm/npm/issues/9359 for details.
  chmod -R u=rwX,go=rX "$pkgdir"

  # npm installs package.json owned by build user
  # https://bugs.archlinux.org/task/63396
  chown -R root:root "$pkgdir"

  install -dm755 "${pkgdir}/usr/share/licenses/${pkgname}"
  ln -s ../../../lib/node_modules/eslint/LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"

  # Experimental dedup
  cd "$pkgdir"/usr/lib/node_modules/$pkgname/node_modules
  for dep in acorn; do
    rm -r $dep;
  done
}
