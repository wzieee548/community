# Maintainer: David Runge <dvzrv@archlinux.org>
# Contributor: Felix Yan <felixonmars@archlinux.org>

_name=identify
pkgname=python-identify
pkgver=1.4.21
pkgrel=1
pkgdesc="File identification library for Python"
arch=('any')
url="https://github.com/chriskuehl/identify"
license=('MIT')
depends=('python')
makedepends=('python-setuptools')
checkdepends=('python-editdistance' 'python-pytest')
# no tests in pypi sdist tarball:
# https://github.com/chriskuehl/identify/issues/95
# source=("https://files.pythonhosted.org/packages/source/${_name::1}/${_name}/${_name}-${pkgver}.tar.gz")
source=("${_name}-${pkgver}.tar.gz::https://github.com/chriskuehl/${_name}/archive/v${pkgver}.tar.gz")
sha512sums=('f991fdb15e1234763bda480613760e118bcca3a280254405210980c1375a54058cea33386da73eba73e13c61e1bd12f7f406e7e7d904ff8e67dd8725abcb246a')

prepare() {
  mv -v "${_name}-${pkgver}" "$pkgname-$pkgver"
}

build() {
  cd "$pkgname-$pkgver"
  python setup.py build
}

check() {
  cd "$pkgname-$pkgver"
  export PYTHONPATH="build:${PYTHONPATH}"
  pytest -v
}

package() {
  cd "$pkgname-$pkgver"
  python setup.py install --skip-build \
    --optimize=1 \
    --prefix=/usr \
    --root="${pkgdir}"
  install -vDm 644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
  install -vDm 644 README.md -t "${pkgdir}/usr/share/doc/${pkgname}"
}
