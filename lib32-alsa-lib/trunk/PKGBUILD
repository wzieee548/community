# Maintainer: David Runge <dvzrv@archlinux.org>
# Contributor: Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>

_name=alsa-lib
pkgname=lib32-alsa-lib
pkgver=1.2.3.1
pkgrel=1
pkgdesc="An alternative implementation of Linux sound support (32 bit)"
arch=('x86_64')
url="https://www.alsa-project.org"
license=('LGPL2.1')
depends=('lib32-glibc' "alsa-lib=${pkgver}")
provides=('libasound.so' 'libatopology.so')
source=("https://www.alsa-project.org/files/pub/lib/${_name}-$pkgver.tar.bz2")
sha512sums=('ed0a81371117a91a5024f3bef5a881ffa9521399eab5e016924669a3f1eba2d6a02efa284b160089bc581717d39d15325ebb2faf162e4d3dabe4c29785a5fb58')
b2sums=('0460ec9449e3936582b29af6ef019ddbe9875090ba722e00074d3d3dfe6214f40c1d96cec8827bb724a52b06abbff1d80dc5fed46f5bb1f03bc5c46a765756fd')

prepare() {
  cd "${_name}-$pkgver"
  autoreconf -vfi
}

build() {
  cd "${_name}-$pkgver"

  export CC='gcc -m32'
  export PKG_CONFIG=i686-pc-linux-gnu-pkg-config

  ./configure --prefix=/usr \
              --libdir=/usr/lib32 \
              --without-debug \
              --disable-python
  make
}

check() {
  cd "${_name}-$pkgver"
  export LD_LIBRARY_PATH="${PWD}/src/.libs/:${LD_LIBRARY_PATH}"
  make -k check
}

package() {
  cd "${_name}-$pkgver"
  make DESTDIR="$pkgdir" install
  rm -r "$pkgdir"/usr/{bin,include,share}
}
