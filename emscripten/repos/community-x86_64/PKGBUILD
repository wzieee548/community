# Maintainer: Sven-Hendrik Haase <svenstaro@gmail.com>
# Contributor: carstene1ns <arch carsten-teibes de> - http://git.io/ctPKG
# Contributor: Stefan Husmann <stefan-husmann@t-online.de>
# Contributor: Vlad Kolotvin <vlad.kolotvin@gmail.com>

pkgname=emscripten
_llvm_commit=3d8149c2a1228609fd7d7c91a04681304a2f0ca9
pkgver=1.39.18
pkgrel=3
pkgdesc="LLVM-based project that compiles C and C++ into highly-optimizable JavaScript for the web"
arch=('x86_64')
url="http://emscripten.org"
license=('custom')
depends=('nodejs' 'python' 'binaryen' 'which')
makedepends=('cmake' 'libxml2' 'git' 'ninja')
optdepends=('java-environment: for using clojure'
            'ruby: for using websockify addon'
            'cmake: for emcc --show-ports')
install=emscripten.install
# Get commit SHAs from here:
# https://chromium.googlesource.com/emscripten-releases/+/refs/heads/master/DEPS
source=("git+https://github.com/kripken/emscripten#tag=$pkgver"
        git+https://github.com/llvm/llvm-project.git#commit=$_llvm_commit
        "emscripten.sh"
        emscripten-config
        libcxxabi-include-libunwind.patch)
sha512sums=('SKIP'
            'SKIP'
            'f7af34461958494c6a9ebd86f5ac63bb76547a2779950cced520b2252e581316149906565f2d8eb8aba47010c1ece5bb27987c6f7286a4ef18eeee8ecad1dae9'
            '1d60b8942f3487c034b7fadb5ab1aeb603131611331ae1c9e8364180ca3a5fadb11f39a30c7f6167080b5e8ba0b1588a3bf42d8904807201cd8c5d6df0653419'
            'b124ff6110810e3190bf05deda478c6fef044ff55a435df978fdb7ff7b4f312186add48cb99946b67a2467f7e28855e36606209c3c4dcee2898762ccc2e4c2ed')

prepare() {
  cd emscripten

  patch -Np1 --no-backup-if-mismatch -i "$srcdir"/libcxxabi-include-libunwind.patch
}

build() {
  cd llvm-project/llvm

  # Inspired from https://github.com/WebAssembly/waterfall/blob/db2ea5eeb11b74cce9b9459be0cc88807744b1b5/src/build.py#L868
  cmake . \
    -Bbuild \
    -GNinja \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_SKIP_RPATH=YES \
    -DLLVM_TARGETS_TO_BUILD="X86;WebAssembly" \
    -DLLVM_BUILD_RUNTIME=OFF \
    -DLLVM_TOOL_LTO_BUILD=ON \
    -DLLVM_INSTALL_TOOLCHAIN_ONLY=ON \
    -DLLVM_INCLUDE_EXAMPLES=OFF \
    -DLLVM_INCLUDE_TESTS=OFF \
    -DLLVM_ENABLE_PROJECTS="lld;clang" \
    -DCLANG_INCLUDE_TESTS=OFF
  ninja -C build
}

package() {
  # Install LLVM stuff according to https://github.com/emscripten-core/emscripten/blob/master/docs/packaging.md
  install -d "$pkgdir"/usr/lib
  cp -r "$srcdir"/llvm-project/llvm/build/bin "$pkgdir"/usr/lib/emscripten-llvm

  # Install emscripten
  cd emscripten
  make DESTDIR="$pkgdir"/usr/lib/emscripten install
  install -Dm644 "$srcdir"/emscripten-config "$pkgdir"/usr/lib/emscripten/.emscripten

  install -d "$pkgdir"/usr/share/doc
  ln -s /usr/lib/emscripten/site/source/docs "$pkgdir"/usr/share/doc/$pkgname
  install -Dm755 "$srcdir"/emscripten.sh "$pkgdir"/etc/profile.d/emscripten.sh
  install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}
