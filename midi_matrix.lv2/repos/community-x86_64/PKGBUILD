# Maintainer: David Runge <dvzrv@archlinux.org>

pkgname=midi_matrix.lv2
pkgver=0.24.0
pkgrel=1
pkgdesc="A plugin for MIDI channel filtering, multiplication and rerouting"
arch=('x86_64')
url="https://open-music-kontrollers.ch/lv2/midi_matrix/"
license=('Artistic2.0')
groups=('lv2-plugins' 'pro-audio')
depends=('glibc' 'libglvnd' 'libx11')
makedepends=('glu' 'lv2' 'meson' 'sord')
checkdepends=('lv2lint')
source=("https://git.open-music-kontrollers.ch/lv2/${pkgname}/snapshot/${pkgname}-${pkgver}.tar.xz")
sha512sums=('e5b18f94ea3e1d27d103739c979fc57609367a0630a62e9727767fe2fdf4efbb9027060198a91e2b6a6ca83195ef88540a5275dc967067a2c9608b1fa6cb7eac')

prepare() {
  cd "$pkgname-$pkgver"
  mkdir -v build
}

build() {
  cd "$pkgname-$pkgver"
  arch-meson --prefix=/usr \
             --libdir=lib \
             --buildtype=release \
             build
  ninja -C build
}

check() {
  cd "$pkgname-$pkgver"
  # check fails due to currently broken lv2_validate in lv2
  #  meson test -C build
  lv2lint -Mpack -I "build/" "http://open-music-kontrollers.ch/lv2/midi_matrix#channel_filter"
}


package() {
  cd "$pkgname-$pkgver"
  DESTDIR="${pkgdir}" meson install -C build
  # docs
  install -vDm 644 {ChangeLog,README.md} \
    -t "${pkgdir}/usr/share/doc/${pkgname}"
}
