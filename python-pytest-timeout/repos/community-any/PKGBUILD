# Maintainer: Felix Yan <felixonmars@archlinux.org>

pkgbase=python-pytest-timeout
pkgname=('python-pytest-timeout' 'python2-pytest-timeout')
pkgver=1.4.1
pkgrel=1
pkgdesc='py.test plugin to abort hanging tests'
arch=('any')
license=('MIT')
url='https://bitbucket.org/pytest-dev/pytest-timeout'
makedepends=('python-pytest' 'python2-pytest')
checkdepends=('python-pexpect' 'python2-pexpect' 'python-pytest-cov' 'python2-pytest-cov')
source=("https://github.com/pytest-dev/pytest-timeout/archive/$pkgver/$pkgname-$pkgver.tar.gz")
sha512sums=('95cd8a3cff73a59eca64adcbb1b3a72dcdbd45760ca76a818ef3dcf4b192fd0bc82e02ce7d9f7bfbfa003d4fd1e16481bc4e94e1086cec56b81a8924f983b275')

prepare() {
  cp -a pytest-timeout-$pkgver{,-py2}
}

build() {
  cd "$srcdir"/pytest-timeout-$pkgver
  python setup.py build

  cd "$srcdir"/pytest-timeout-$pkgver-py2
  python2 setup.py build
}

check() {
  # Hack entry points by installing it

  cd "$srcdir"/pytest-timeout-$pkgver
  python setup.py install --root="$PWD/tmp_install" --optimize=1
  PYTHONPATH="$PWD/tmp_install/usr/lib/python3.8/site-packages:$PYTHONPATH" py.test

  cd "$srcdir"/pytest-timeout-$pkgver-py2
  python2 setup.py install --root="$PWD/tmp_install" --optimize=1
  PYTHONPATH="$PWD/tmp_install/usr/lib/python2.7/site-packages:$PYTHONPATH" py.test2
}

package_python-pytest-timeout() {
  depends=('python-pytest')

  cd pytest-timeout-$pkgver
  python setup.py install --root="$pkgdir"/ --optimize=1
  install -D -m644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

package_python2-pytest-timeout() {
  depends=('python2-pytest')

  cd pytest-timeout-$pkgver-py2
  python2 setup.py install --root="$pkgdir"/ --optimize=1
  install -D -m644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

# vim:set ts=2 sw=2 et:
