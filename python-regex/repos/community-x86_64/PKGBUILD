# Maintainer: Jelle van der Waa <jelle@archlinux.org>
# Maintainer: Eli Schwartz <eschwartz@archlinux.org>
# Contributor: Nikola Milinković <nikmil@gmail.com>
# Submitter: Xiao-Long Chen <chenxiaolong@cxl.epac.to>

_pkgbase=regex
pkgbase=python-regex
pkgname=('python-regex' 'python2-regex')
pkgver=2020.6.8
pkgrel=1
pkgdesc="Alternative python regular expression module."
arch=('x86_64')
url="https://bitbucket.org/mrabarnett/mrab-regex"
license=('Python')
makedepends=('python-setuptools' 'python2-setuptools')
options=(!emptydirs)
source=("https://files.pythonhosted.org/packages/source/r/${_pkgbase}/${_pkgbase}-${pkgver}.tar.gz")
sha256sums=('e9b64e609d37438f7d6e68c2546d2cb8062f3adb27e6336bc129b51be20773ac')
b2sums=('7b773b72496e7943c3c79b9e4747994a91781ba0b5f85a41ffc1282054eb600710f32617338b6e2130026f2aeb69ba4d625feb202b6b916238fc1442e8f698e8')

build() {
  cd "regex-${pkgver}"

  python setup.py build
  python2 setup.py build
}

check() {
  cd "regex-${pkgver}"

  pushd build/lib.linux-${CARCH}-3*/
  python -m unittest regex/test_regex.py
  popd

  pushd build/lib.linux-${CARCH}-2*/
  python2 -m unittest regex.test_regex
  popd
}

package_python2-regex() {
  depends=('python2')
  pkgdesc="Alternative python regular expression module. (python2 version)"

  cd "regex-${pkgver}"
  python2 setup.py install --root="${pkgdir}/" --optimize=1 --skip-build

  sed -n '1,/^$/p' regex_2/regex.py | install -Dm644 /dev/stdin "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE
}

package_python-regex() {
  depends=('python')
  pkgdesc="Alternative python regular expression module. (python3 version)"

  cd "regex-${pkgver}"
  python setup.py install --root="${pkgdir}/" --optimize=1 --skip-build

  sed -n '1,/^$/p' regex_3/regex.py | install -Dm644 /dev/stdin "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE
}
