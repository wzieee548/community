# Maintainer: kpcyrd <kpcyrd[at]archlinux[dot]org>
# Contributor: Aram Drevekenin <aram@poor.dev>

pkgname=bandwhich
pkgver=0.15.0
pkgrel=1
pkgdesc='Terminal bandwidth utilization tool'
url='https://github.com/imsnif/bandwhich'
arch=('x86_64')
license=('MIT')
depends=('gcc-libs')
makedepends=('cargo')
source=(https://github.com/imsnif/${pkgname}/archive/${pkgver}/${pkgname}-${pkgver}.tar.gz)
sha512sums=('dc9a9a485b03f80d74bd5024252a75c48c5d40f3dd69d262a2f24c7ce80e38d09a718b452640d0191e0bea624ec321f170f312051eae249ba016a9daaf1c27fe')
b2sums=('5daf3e994d723ab16202a9dbb6fa5f246a5ee73bb7cfc0ee71ef1d6e463196c1a6c8e34575b23a11f11b02a710a014f6e683029960a8ee6a382360a174c92ecf')

build() {
  cd "${pkgname}-${pkgver}"
  cargo build --release --locked
}

check() {
  cd "${pkgname}-${pkgver}"
  cargo test --release --locked
}

package() {
  cd "${pkgname}-${pkgver}"
  install -Dm 755 "target/release/${pkgname}" "${pkgdir}/usr/bin/${pkgname}"
  install -Dm 644 "docs/bandwhich.1" -t "${pkgdir}/usr/share/man/man1"
  install -Dm 644 LICENSE.md "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}

# vim: ts=2 sw=2 et:
