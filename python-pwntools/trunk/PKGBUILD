# Maintainer: kpcyrd <kpcyrd[at]archlinux[dot]org>
# Maintainer: Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor: Ding Xiao <tinocodfcdsa10@mails.tsinghua.edu.cn>
# Contributor: Firmy <firmianay@gmail.com>

pkgname=python-pwntools
_pyname=pwntools
pkgver=4.1.1
pkgrel=1
pkgdesc='CTF framework and exploit development library'
url='https://github.com/Gallopsled/pwntools'
arch=('any')
license=('MIT' 'GPL2' 'BSD')
depends=('python'
         'python-mako'
         'python-pyelftools'
         'python-capstone'
         'ropgadget'
         'python-pyserial'
         'python-requests'
         'python-pip'
         'python-pygments'
         'python-pysocks'
         'python-dateutil'
         'python-packaging'
         'python-psutil'
         'python-intervaltree'
         'python-sortedcontainers'
         'python-unicorn'
         'python-paramiko'
         'ropgadget'
         'python-setuptools')
makedepends=('pandoc')
source=(https://github.com/Gallopsled/pwntools/archive/${pkgver}/${pkgname}-${pkgver}.tar.gz
        pwn.conf
        # cherry-pick patch for [update] config section
        https://github.com/Gallopsled/pwntools/commit/433d92619b98b916bee21865df066e005f28dc80.patch)
sha256sums=('da3a32aa5deacf906a96c559b3b237e3f79b76afe76fe87ca66892e58ac83b8f'
            '50916e4e45d026422cd6bfe8de50e7246c61b0b1e0007571e7c04a994282fe89'
            '25a4bd5edc81e3f31200b8104392330cb4f5f30868c474ac55b99b5e4fdc73a0')
b2sums=('e30782b4872c42113e3307dfd493364322681c7cfdc9ba1345207598420a25fdb00be5448e4adfc648798c712b0721b060c0d6fdffa991061d43d5024a81dc1b'
        'cac0a12eea8abd3d2ee21632e7f2281e7e35c2d7839cfbcba83ae5d0e8f427e9cc2462a6d17d187252b99d9dbbb14393b9d5a63996c42043f553e9b66bfe9e07'
        'b41a77d6e3a799ff937c1b04713919d5357fa371a0f730ddc83ff8253d463cb86b89bcf705724fd68a9c4fb8567821cfbc85be6723d6b43d916cff9385413363')

prepare() {
  cd ${_pyname}-${pkgver}
  sed 's|>=1.0.2rc1,<1.0.2rc4||' -i setup.py

  patch --forward --strip=1 --input="${srcdir}/433d92619b98b916bee21865df066e005f28dc80.patch"
}

build() {
  cd ${_pyname}-${pkgver}
  python setup.py build
}

check() {
  cd "${_pyname}-${pkgver}"
  PYTHONPATH=. PWNLIB_NOTERM=true python -c 'import pwn'
}

package() {
  cd ${_pyname}-${pkgver}
  python setup.py install -O1 --root="${pkgdir}" --skip-build --only-use-pwn-command
  install -Dm 644 "${srcdir}/pwn.conf" -t "${pkgdir}/etc"
  install -Dm 644 LICENSE-pwntools.txt -t "${pkgdir}/usr/share/licenses/${pkgname}"
  rm -f "${pkgdir}"/usr/lib/python*/site-packages/*.{txt,md}
}

# vim:set ts=2 sw=2 et:
