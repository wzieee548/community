# Maintainer: Felix Yan <felixonmars@archlinux.org>

_hkgname=selective
pkgname=haskell-selective
pkgver=0.4.1
pkgrel=8
pkgdesc="Selective applicative functors"
url="https://github.com/snowleopard/selective"
license=('MIT')
arch=('x86_64')
depends=('ghc-libs')
makedepends=('ghc' 'haskell-quickcheck' 'haskell-tasty' 'haskell-tasty-expected-failure'
             'haskell-tasty-quickcheck')
source=(https://hackage.haskell.org/packages/archive/${_hkgname}/${pkgver}/${_hkgname}-${pkgver}.tar.gz)
sha512sums=('9155bdea49bdb579244d2e47fa2cb44a647d75dbfcab702880cbd139c71111fa2184fedceaf72892fd76c96820e22b871aff7b242349fdd17569d4f9d10631a1')

prepare() {
    cd $_hkgname-$pkgver
    echo -e "import Distribution.Simple\nmain = defaultMain" > Setup.hs
}

build() {
    cd $_hkgname-$pkgver    
    
    runhaskell Setup configure -O --enable-shared --enable-executable-dynamic --disable-library-vanilla \
        --prefix=/usr --docdir=/usr/share/doc/$pkgname --enable-tests \
        --dynlibdir=/usr/lib --libsubdir=\$compiler/site-local/\$pkgid
    runhaskell Setup build $MAKEFLAGS
    runhaskell Setup register --gen-script
    runhaskell Setup unregister --gen-script
    sed -i -r -e "s|ghc-pkg.*update[^ ]* |&'--force' |" register.sh
    sed -i -r -e "s|ghc-pkg.*unregister[^ ]* |&'--force' |" unregister.sh
}

check() {
    cd $_hkgname-$pkgver
    runhaskell Setup test
}

package() {
    cd $_hkgname-$pkgver
    
    install -D -m744 register.sh "$pkgdir"/usr/share/haskell/register/$pkgname.sh
    install -D -m744 unregister.sh "$pkgdir"/usr/share/haskell/unregister/$pkgname.sh
    runhaskell Setup copy --destdir="$pkgdir"
    install -D -m644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
    rm -f "$pkgdir"/usr/share/doc/$pkgname/LICENSE
}
