# Maintainer: David Runge <dvzrv@archlinux.org>

pkgname=lv2lint
pkgver=0.6.0
pkgrel=1
pkgdesc="Check whether a given LV2 plugin is up to the specification"
arch=('x86_64')
url="https://open-music-kontrollers.ch/lv2/lv2lint/"
license=('Artistic2.0')
depends=('glibc' 'libelf')
makedepends=('curl' 'lilv' 'lv2' 'meson')
source=("https://git.open-music-kontrollers.ch/lv2/${pkgname}/snapshot/${pkgname}-${pkgver}.tar.xz")
sha512sums=('df8e53b55ff88f088ad8290be765fc33fded72359541e9f5342e7282445d3f310c0f94694450d3d42ad26afd34ae39004b5db33c85bd6bfc50248329082a639e')

build() {
  cd "$pkgname-$pkgver"
  meson --prefix=/usr \
        -Donline-tests=true \
        -Delf-tests=true \
        build
  ninja -C build
}

check() {
  cd "$pkgname-$pkgver"
  ninja -C build test
}

package() {
  depends+=('libcurl.so' 'liblilv-0.so')
  cd "$pkgname-$pkgver"
  DESTDIR="$pkgdir/" ninja -C build install
  install -vDm 644 {ChangeLog,README.md,TODO} \
    -t "${pkgdir}/usr/share/doc/${pkgname}"
}
