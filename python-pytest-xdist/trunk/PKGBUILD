# Maintainer: Felix Yan <felixonmars@archlinux.org>

pkgbase=python-pytest-xdist
pkgname=('python-pytest-xdist' 'python2-pytest-xdist')
pkgver=1.32.0
pkgrel=1
pkgdesc='py.test xdist plugin for distributed testing and loop-on-failing modes'
arch=('any')
license=('MIT')
url='https://bitbucket.org/pytest-dev/pytest-xdist'
makedepends=('python-pytest' 'python2-pytest' 'python-setuptools_scm' 'python2-setuptools_scm'
             'python-execnet' 'python2-execnet' 'python-pytest-forked' 'python2-pytest-forked'
             'python-filelock' 'python2-filelock')
source=("$pkgbase-$pkgver.tar.gz::https://github.com/pytest-dev/pytest-xdist/archive/v$pkgver.tar.gz")
sha512sums=('4a6458d789130712fe521a0786f7a6143d28b99b80497f4d87c6600e879533bbe19898782f1dd95ffe88afd11f509dd6042bc6f057b5a4d751d8e572e0f83b6e')

prepare() {
  cp -a pytest-xdist-$pkgver{,-py2}

  export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
}

build() {
  cd "$srcdir"/pytest-xdist-$pkgver
  python setup.py build

  cd "$srcdir"/pytest-xdist-$pkgver-py2
  python2 setup.py build
}

check() {
  # Hack entry points by installing it

  cd "$srcdir"/pytest-xdist-$pkgver
  python setup.py install --root="$PWD/tmp_install" --optimize=1
  PYTHONPATH="$PWD/tmp_install/usr/lib/python3.8/site-packages:$PYTHONPATH" py.test

  cd "$srcdir"/pytest-xdist-$pkgver-py2
  python2 setup.py install --root="$PWD/tmp_install" --optimize=1
  PYTHONPATH="$PWD/tmp_install/usr/lib/python2.7/site-packages:$PYTHONPATH" py.test2
}

package_python-pytest-xdist() {
  depends=('python-pytest-forked' 'python-execnet')

  cd pytest-xdist-$pkgver
  python setup.py install --root="$pkgdir" --optimize=1
  install -D -m644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

package_python2-pytest-xdist() {
  depends=('python2-pytest-forked' 'python2-execnet')

  cd pytest-xdist-$pkgver-py2
  python2 setup.py install --root="$pkgdir" --optimize=1
  install -D -m644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}
# vim:set ts=2 sw=2 et:
